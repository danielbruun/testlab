param($variables, $name)

#Import variables
. $variables

#Remove autologin from registry
clear-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name DefaultUserName
remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name DefaultPassword
remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name AutoAdminLogon
remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name ForceAutoLogon

Remove-Item -Path $credFile

# Create VMs
if($name -eq $nestedVm -or $name -eq $nucName){

    #Hyper-V setup and install
    $VirtualMachine = "C:\hyperv\Virtual Machines"
    $VirtualHardDisk = "C:\hyperv\Virtual Harddisk"
    $InstallMedia = 'C:\Users\Administrator\Desktop\Oppstart\17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso'
    # $adapterName = $(Get-NetAdapter).name
    $adapterName = "Ethernet"

    # Setup on nested hyper-v Vm
    if ($name -eq $nestedVm) {
        $VMName = $underVm
        $switch = $hyperSwitch
        $mem = 2GB
        $hdd = 30GB   
    }

    # Setup on master Vm
    if ($name -eq $nucName) {
        $VMName = $nestedVm
        $switch = $nucSwitch
        $mem = 16GB
        $hdd = 200GB
    }

    # Create Virtual Switch
    New-VMSwitch -name $switch -NetAdapterName $adapterName -AllowManagementOS $true

    # Create New Virtual Machine
    New-VM -Name $VMName -MemoryStartupBytes $mem -Generation 2 -NewVHDPath "$VirtualHardDisk\$VMName.vhdx" -NewVHDSizeBytes $hdd -Path "$VirtualMachine\$VMName" -SwitchName $Switch

    # Add DVD Drive to Virtual Machine
    Add-VMScsiController -VMName $VMName
    Add-VMDvdDrive -VMName $VMName -ControllerNumber 1 -ControllerLocation 0
    # Add-VMDvdDrive -VMName $VMName -ControllerNumber 1 -ControllerLocation 0 -Path $InstallMedia----->  Dersom DVD er tilgjengelig

    # Mount Installation Media
    $DVDDrive = Get-VMDvdDrive -VMName $VMName

    # Configure Virtual Machine to Boot from DVD
    Set-VMFirmware -VMName $VMName -FirstBootDevice $DVDDrive

    # Start Vm
    Start-Vm -Name $VMName
    }

#Remove local repository
Remove-Item -Path $path -Recurse -Force

    Start-Sleep -s 60 # Check for errors

