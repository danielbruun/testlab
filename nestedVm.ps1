$nestedVm = "servah"
$isoPath = "C:\Users\Administrator\Desktop\Oppstart\17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso"

#Open for nesting?
Set-VMProcessor -VMName $nestedVm -ExposeVirtualizationExtensions $true

#Set networksadapter for nested vm
Get-VMNetworkAdapter -VMName $nestedVm | Set-VMNetworkAdapter -MacAddressSpoofing On

#Enable guest service on VM. For Copy-VmFile to work
Enable-VMIntegrationService -Name 'Guest Service Interface' -VMName $nestedVm
Copy-VMFile $nestedVm -SourcePath $isoPath -DestinationPath $isoPath -CreateFullPath -FileSource Host

