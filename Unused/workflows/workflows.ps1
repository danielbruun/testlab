workflow foreachpsptest {

   param([string[]]$computers)

   foreach –parallel ($computer in $computers){

    sequence {

      Get-WmiObject -Class Win32_ComputerSystem -PSComputerName $computer

      Get-WmiObject –Class Win32_OperatingSystem –PSComputerName $computer

      $disks = Get-WmiObject -Class Win32_LogicalDisk `

         -Filter “DriveType = 3” –PSComputerName $computer
    }
   }
}
