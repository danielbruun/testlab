New-Item -Path C:\Shared -ItemType Directory

New-SMBShare –Name “Shared” –Path “C:\Shared” `
 –ContinuouslyAvailable `
 –FullAccess domain\admingroup  ` 
 -ChangeAccess domain\deptusers `
 -ReadAccess “domain\authenticated users”