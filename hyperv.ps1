param($nestedVm)
 
# Install the Hyper-V hypervisor and all tools
Install-WindowsFeature -Name Hyper-V -IncludeManagementTools


#Create Hyper-V VMs
$VMName = $nestedVm
$adapterName = $(Get-NetAdapter).name
$switch = 'ServahSwitch'
$VirtualMachine = "C:\hyperv\Virtual Machines"
$VirtualHardDisk = "C:\hyperv\Virtual Harddisk"
$InstallMedia = 'C:\Users\Administrator\Desktop\Oppstart\17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso'
$mem = 2GB
$hdd = 40GB


# Create Virtual Switch
New-VMSwitch -name $switch -NetAdapterName $adapterName -AllowManagementOS $true

# Create New Virtual Machine
New-VM -Name $VMName -MemoryStartupBytes $mem -Generation 2 -NewVHDPath "$VirtualHardDisk\$VMName.vhdx" -NewVHDSizeBytes $hdd -Path "$VirtualMachine" -SwitchName $Switch

# Add DVD Drive to Virtual Machine
Add-VMScsiController -VMName $VMName
Add-VMDvdDrive -VMName $VMName -ControllerNumber 1 -ControllerLocation 0 -Path $InstallMedia

# Mount Installation Media
$DVDDrive = Get-VMDvdDrive -VMName $VMName

# Configure Virtual Machine to Boot from DVD
Set-VMFirmware -VMName $VMName -FirstBootDevice $DVDDrive



#Start chocogit on VM after deployment?