workflow brochacho {

    #Kjør som -jobname bro 
    new-item -type file -name b4 -value "før restart"    

    Restart-Computer -Wait
 
    new-item -type file -name test -value "after restart"
    
    Unregister-ScheduledJob -Name broflow
    get-job | remove-job

}

# -------------------------------------------------------------------------
# Use the New-JobTrigger cmdlet to create an "At startup" trigger
# to resume the suspended job.
# Replace <Password> with a password of a administrator
# on the local machine. 
# -------------------------------------------------------------------------
$adm = (Get-Content -Path "C:\Users\Administrator\Desktop\cred.txt" -TotalCount 2)[0]
$foo = (Get-Content -Path "C:\Users\Administrator\Desktop\cred.txt" -TotalCount 2)[-1]
$pwd = ConvertTo-SecureString -String $foo -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential($adm, $pwd)
Set-AutoLogon -DefaultUsername $adm -DefaultPassword $pwd -AutoLogonCount "1"
$AtStartup = New-JobTrigger -AtStartup
Register-ScheduledJob -Name broflow `
                      -Credential $cred `
                      -Trigger $AtStartup `
                      -ScriptBlock {Import-Module PSWorkflow; `
                          Get-Job -Name bro `
                          | Resume-Job}

brochacho -jobname bro