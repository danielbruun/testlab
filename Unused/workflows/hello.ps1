workflow workflowz {
    $msgtest = "Hello"
    #$msgtest.toupper()
    $msgtest = inlinescript {($using:msgtest).toupper()}
    new-item -value $msgtest -name workflow.txt -Path C:\Users\Administrator\Desktop

    #$AtStartup = New-JobTrigger -AtStartup

    #Register-ScheduledJob -Name ResumeWorkflow -Trigger $AtStartup -ScriptBlock {Import-Module PSWorkflow; Get-Job ComputerSetup -State Suspended | Resume-Job}

    Restart-Computer -Wait
    new-item -value $msgtest -name restart.txt -Path C:\Users\Administrator\Desktop

# Run the workflow. It is suspended when the computer restarts.
#New-ComputerSetup -JobName ComputerSetup

# Verify that the workflow was resumed and completed successfully
#Import-Module PSWorkflow
#Get-Job ComputerSetup

# Delete the scheduled job
#Unregister-ScheduledJob -Name ComputerSetup
}