param($variables)

# Import variables dot sourced
. $variables

# Set resolution
Set-DisplayResolution -Width 1920 -Height 1080 -Force
 
# Check if computer is a Vm and is not a copy of NucBro in the cloud
$name = (Get-ComputerInfo).csname
$computerModel = (Get-Wmiobject -Computer LocalHost win32_computersystem).Model
if ($computerModel -eq "Virtual Machine" -and $name -ne $nucName) {
      # Current VM name
      $nameHost = (Get-Item "HKLM:\SOFTWARE\Microsoft\Virtual Machine\Guest\Parameters").GetValue("PhysicalHostName")
      
      # Rename nested Hyper-V Vm
      if ($nameHost -eq $nucName) {
            $name = $nestedVm
            Rename-Computer -NewName $name
      }
      # Rename nested plain Vm
      elseif ($nameHost -eq $nestedVm) {
            $name = $underVm
            Rename-Computer -NewName $name
      }
      # Fill required variable, and skip Hyper-v install
      else { 
            $name = "NoHyperv"
      }

} else {
      # Rename NUC if not already named
      if ($name -ne $nucName) {
            $name = $nucName
            Rename-Computer -NewName $name
      }

      # Create fileshare
      New-SmbShare -Path C:\Shared -Name Shared -ReadAccess Everyone
}

# Get username and password from file, and create Credential-object
$Username = (Get-Content $credfile)[0]
$Password = (Get-Content $credfile)[-1] | ConvertTo-SecureString
$credential = New-Object System.Management.Automation.PSCredential($Username, $Password)

# HKLM gjelder alle, hkcu gjelder bare en spesifikk bruker 
# Run hver oppstart, runonce slettes etter en kjøring regardless av suksess eller ikke. Bruk ! i navnet for å ikke slette dersom den feiler.
# Bruk * for å kjøre den selv i safe-mode. EKs !Navn
Set-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" `
-Name "!After-restart" `
-Value "c:\WINDOWS\system32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy unrestricted -file $cleanUp -name $name -variables $variables"

# Setting registry keys for autologon. #Registry keys reset or removed after RunOnce-script has run after restart
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name DefaultUserName -Value $credential.Username
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name DefaultPassword -Value $credential.GetNetworkCredential().Password
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name AutoAdminLogon -Value "1"
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name ForceAutoLogon -Value "1"

# Remove Internet Explorer and remove startup task for server manager
Disable-WindowsOptionalFeature -FeatureName Internet-Explorer-Optional-amd64 -Online -NoRestart
Get-ScheduledTask -TaskName Servermanager | Disable-ScheduledTask

if($name -eq $nestedVm -or $name -eq $nucName){

      # Install the Hyper-V hypervisor and all tools
      Install-WindowsFeature -Name Hyper-V -IncludeManagementTools

     }
     
# Set keyboard layout
Set-WinUserLanguageList -LanguageList nb -Force

Restart-Computer