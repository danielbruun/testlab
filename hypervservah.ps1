param($nestedVm)

# Install the Hyper-V hypervisor and all tools
Install-WindowsFeature -Name Hyper-V -IncludeManagementTools


#Create Hyper-V VMs
$VMName = $nestedVm
$adapterName = $(Get-NetAdapter).name
$switch = 'VMware Virtual Ethernet Adapter for VMnet8 - Virtual Switch'
$VirtualMachine = "C:\hyperv\Virtual Machines"
$VirtualHardDisk = "C:\hyperv\Virtual Harddisk"
$InstallMedia = 'C:\Users\Administrator\Desktop\Oppstart\17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso'
$mem = 16GB
$hdd = 120GB

# # Create Virtual Switch
# New-VMSwitch -name $switch -NetAdapterName $adapterName -AllowManagementOS $true

# Create New Virtual Machine
New-VM -Name $VMName -MemoryStartupBytes $mem -Generation 2 -NewVHDPath "$VirtualHardDisk\$VMName.vhdx" -NewVHDSizeBytes $hdd -Path "$VirtualMachine" -SwitchName $Switch

# Add DVD Drive to Virtual Machine
Add-VMScsiController -VMName $VMName
Add-VMDvdDrive -VMName $VMName -ControllerNumber 1 -ControllerLocation 0 -Path $InstallMedia

# Mount Installation Media
$DVDDrive = Get-VMDvdDrive -VMName $VMName

# Configure Virtual Machine to Boot from DVD
Set-VMFirmware -VMName $VMName -FirstBootDevice $DVDDrive

#Open for nesting?
Set-VMProcessor -VMName $nestedVm -ExposeVirtualizationExtensions $true

#Set networksadapter for nested vm
Get-VMNetworkAdapter -VMName $nestedVm | Set-VMNetworkAdapter -MacAddressSpoofing On

#Enable guest service on VM. For Copy-VmFile to work
Enable-VMIntegrationService -Name 'Guest Service Interface' -VMName $nestedVm

Start-VM -Name $VMName

# After deployment configuration nested Hyper-V
$isoPath = "C:\Users\Administrator\Desktop\Oppstart\17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso"

Copy-VMFile $nestedVm -SourcePath $isoPath -DestinationPath $isoPath -CreateFullPath -FileSource Host