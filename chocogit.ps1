#Start direkte fra Bitbucket
#Invoke-Expression -Command $((Invoke-Webrequest https://bitbucket.org/danielbruun/testlab/raw/HEAD/chocogit.ps1).Content)

#Input credentials
$credential = Get-Credential -Message 'Enter the local administrative username and password' -UserName $env:USERNAME

#Import variables
Invoke-Expression -Command $((Invoke-WebRequest https://bitbucket.org/danielbruun/testlab/raw/HEAD/variables.ps1).Content)

#Credentials to textfile for later use
$credential.UserName | Out-File -FilePath $credfile
ConvertFrom-SecureString $credential.Password | Out-File -FilePath $credfile -Append

#Install chocolatey + apps
Set-ExecutionPolicy Bypass -Scope Process -Force
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco install git -y
choco install vscode -y
choco install firefox -y
choco install bitwarden -y

# Må kjøres med full path uten restart etter installasjon
Set-Location $rootDir
& 'C:\Program Files\Git\cmd\git.exe' clone $repo

Invoke-Expression -Command $nextScript