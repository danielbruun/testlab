#Variables

#Chocogit
$repo = "https://danielbruun@bitbucket.org/danielbruun/testlab.git"
$rootDir= "C:\"
$path = $rootDir + "testlab"
$variables = "$path\variables.ps1"
$cleanUp = "$path\cleanUpsetUp.ps1"
$credFile = "C:\credfile.txt"
$nextScript = "$path\noworkflows.ps1 -variables $variables"

#Hyper-V VM-name check
$nucName = "NucBro"
$nestedVm = "Servah"
$underVm = "DisGuy"

#Hyper-V components
$nucSwitch = "NucSwitch"
$hyperSwitch = "ServahSwitch"

#Specs
$width = 1920
$height = 1080