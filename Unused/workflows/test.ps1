workflow workflowz {
    inlinescript{ Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    choco install git -y
    choco install vscode -y
    choco install firefox -y
    New-ComputerSetup -JobName ComputerSetup
    }
    $AtStartup = New-JobTrigger -AtStartup

    Register-ScheduledJob -Name ResumeWorkflow -Trigger $AtStartup -ScriptBlock {Import-Module PSWorkflow; Get-Job ComputerSetup -State Suspended | Resume-Job}

    Restart-Computer -Wait

    inlinescript {set-location C:\Users\Administrator\Desktop
    git clone https://danielbruun@bitbucket.org/danielbruun/testlab.git
    Unregister-ScheduledJob -Name ComputerSetup
    }
}